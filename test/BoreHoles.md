
---
sidebar_position: 10
slug: Datasets/BuildingInspection
title: Building Inspection
author: Phil Tomlinson
author_title: bayeslife
tags: []    
---
# DataSet
BoreHole

## Id
BoreholeDS

## Name
Borehole Data Set

## Description
Repository of borehole data across Australia and New Zealand

## AureconTaxonomy_InformationClassification
Internal

## DataSources

- Various gInt files
- P:\CIV\10000-001\77_Transportation\GEOTECH\08 Analysis and Design\gINT\Jobs 1)
- \\aurecon.info\shares\AUNTL\Admin\Disciplines\Geotechnical\Geotechnical Data\gINT data
- XPDF Data Feed
- CPT Data Feed
        
## AureconTaxonomy_Practice
Ground & Underground Engineering

## AureconTaxonomy_Capability
Geotechnical Engineering
    