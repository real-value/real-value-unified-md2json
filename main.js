let Processor = require('./index')

async function main(){

  const {streamProcessor} = Processor()
  let data = await streamProcessor(process.stdin)
  console.log(JSON.stringify(data,null,' '))  
}

main()