
// var stream = require('unified-stream')
// var slug = require('remark-slug')
var remark2rehype = require('remark-rehype')
var html = require('rehype-stringify')

var unified = require('unified')
var stream = require('unified-stream')
var markdown = require('remark-parse')
var fs = require('fs')
var retext = require('retext')
var report = require('vfile-reporter')
var Renderer = require('./src/renderer')

function Processor(){

  const renderer  = Renderer()

  var processor = unified()
  .use(markdown)
  .use(renderer.attacher)
  .use(remark2rehype)
  .use(html)
  
  function streamProcessor(inputStream){
      return new Promise((resolve)=>{
          let astream = inputStream.pipe(stream(processor))
          astream.on('end',()=>{
            resolve(renderer.producer())
          })
      })
  }

  function fileProcessor(file){
      let readStream = fs.createReadStream(file)
      return streamProcessor(readStream)
  }

  return {
    streamProcessor,
    fileProcessor
  }

}

module.exports = Processor