# Read markdown file and produces json data structure

## Syntax 

### Hierarchy

Structure of the markdown is

```
Heading1
    Heading2
        Heading3
```

which becomes

```
[
        {
            name: <Heading2>
            items: [
                {
                    name: <Heading3>
                }
            ]
        }
]
```

### Lists

This
```
- one
- two
- thre
```
becomes
```
 "value": [
   {
    "value": "one"
   },
   {
    "value": "two"
   },
   {
    "value": "three"
   }
  ]
```

### Links

Values can be of the form
```
link <source> <relationship> <target>
```
which becomes
```
"value": {
   "source": "foo",
   "relationship": "rel",
   "target": "bar"
  }
```


### Href

Hrefs are current removed
```
[test](./test)
```
becomes
```
test
```

## How to use

```
cat <somemarkddown.md> | node main
```


## Relates to

- [Unified](https://unifiedjs.com/) - Unified is a capability to parse content and transform to other syntax