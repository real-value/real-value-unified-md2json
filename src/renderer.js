
var visit = require('unist-util-visit')

function Renderer(){
    let result = []
    let current = null
    let currentSet = null
    let currentItem = null
    
    let p = false
    let list = null
    let listitem = null

    function producer(){
        return result
    }
    
    function attacher() {
        
        function transformer(tree, file) {

            let FOUND_TOP_HEADING=false

            let stackState = []
            let valueType = null
            
            visit(tree, 
                    //['heading','paragraph'], 
                    visitor)  
            
            

            function visitor(node,i,parent) {          
                //console.log(`${node.type} ${i} ${parent?.type} ${node.value}`)                
                
                if(stackState.length>0){                                        
                        //console.log(`${parent?.type} ${stackState[stackState.length-1]}`)                
                        while(stackState.length>0  && parent.type!==stackState[stackState.length-1]){                            
                             let p = stackState.pop() 
                             current.value = current.value + (p ==='tableCell' ? "</td>" : (p==='tableRow' ? '</tr>' : '</table>'))
                        }
                    }
                
                if(node.type==='heading' && node.depth===1){    
                    FOUND_TOP_HEADING=true                
                }
                if(!FOUND_TOP_HEADING){
                    return
                }    
                
                    if(node.type==='heading'){    
                        p=false
                        list=null
                        listitem=null
                        if(node.depth===1){
                            let setName = node.children[0].value
                            let set = {name: setName,attrs:[],items:[],value:''}
                            result.push(set)                
                            current = currentSet = set
                        }else if(node.depth===2){                
                            let itemName = node.children[0].value
                            let item ={name: itemName,attrs:[],value:''}
                            currentSet.items.push(item)
                            current = currentItem = item
                        }else if (node.depth===3){
                            let attrName = node.children[0].value
                            let attr ={name: attrName,value:''}
                            if(currentItem)
                                currentItem.attrs.push(attr)
                            else if(currentSet)
                                currentSet.attrs.push(attr)
                            current = attr
                        }
                    }else if(node.type==='list'){
                        list=[]
                        listitem=null
                        current.value = list
                    }else if(node.type==='listItem'){
                        listitem  = current = {value:''}
                        list.push(current)
                    }else if(node.type==='paragraph'){                                                
                        if(current){
                            //console.log(current)
                            p=true
                            current.value = current.value ? current.value + '\n' : current.value
                        }                            
                    }else if(node.type==='inlineCode'){
                        current.value = current.value + node.value
                    }else if(node.type==='link'){                    
                        // console.log(`${node.type} ${i} ${parent?.type} ${node.value}`)                
                        // console.log(node)
                        // current.value = current.value + node.children[0].value
                        current.value = node.url
                        current.valueType='link'
                    }else if(node.type==='text'){
                        
                        if(p){
                            //console.log(node.value)
                            if(node.value.indexOf('link')===0){
                                let parts = node.value.split(' ')
                                current.value = {source: parts[1], relationship: parts[2], target: parts[3]}
                            }else
                                current.value = (current.value + ' '+ node.value).trim().replace('  ',' ')
                        }
                            
                    }else if(node.type==='emphasis'){
                        //console.log(node)
                    }else if(node.type==='image'){
                        if(current){                            
                            p=true
                            current.value = node.url
                            current.valueType='url'
                        }
                    }else if(node.type==='table'){
                        p=true
                        current.value = current.value + "<table>"
                        stackState.push(node.type)
                        current.valueType='html'
                    }else if(node.type==='tableRow'){
                        current.value = current.value + "<tr>"
                        stackState.push(node.type) 
                    }else if(node.type==='tableCell'){                        
                        stackState.push(node.type)                     
                        current.value = current.value + "<td>"                        
                    }else {
                        //console.log(node)
                    }
            }
        }
        return transformer
    }
    
    return {
        attacher,
        producer
    }
}

module.exports =  Renderer